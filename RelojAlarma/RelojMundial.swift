//
//  RelojMundial.swift
//  RelojAlarma
//
//  Created by Javier Ramon Gil on 14/4/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit
import AudioToolbox

class RelojMundial: UIViewController,
        UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet weak var citiesPicker: UIPickerView!
    @IBOutlet weak var label: UILabel!
    private let citiesNames = [
        "Berlín", "Londres", "Madrid", "Nueva York", "Tokyo"]

    @IBOutlet weak var labelHour: UILabel!
    var timeAlarm = " "
    private var soundID: SystemSoundID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 3600 * 2)
        label.text = dateFormatter.stringFromDate(date)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK:-
    // MARK: Picker Data Source Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1 }
    func pickerView(pickerView: UIPickerView,
        numberOfRowsInComponent component: Int) -> Int {
        return citiesNames.count
    }
    
    // MARK: Picker Delegate Methods
    func pickerView(pickerView: UIPickerView,
        titleForRow row: Int,
        forComponent component: Int) -> String! {
        return citiesNames[row]
    }
    
    func pickerView(pickerView: UIPickerView,
        didSelectRow row: Int,
        inComponent component: Int) {
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        if row == 0 {
            dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 3600 * 2)
        }
        if row == 1 {
            dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 3600 * 1)
        }
        if row == 2 {
            dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 3600 * 2)
        }
        if row == 3 {
            dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 3600 * (-4))
        }
        if row == 4 {
            dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 3600 * 9)
        }
        
        self.label.text = dateFormatter.stringFromDate(date)
        var str = self.label.text
        loadFromPlist()
        
            if str == timeAlarm{
                self.playSound()
                let alert = UIAlertController(title: "Aviso", message: "Las horas de los pickers coinciden: \(timeAlarm)", preferredStyle: .Alert)
                let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(action)
                presentViewController(alert, animated: true, completion: nil)
                
            }
            
        writeToPlist(str!)

    }
    
    func writeToPlist(str:String){
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let path = documentDirectory.stringByAppendingPathComponent("data.plist")
        
        var array:NSArray = ["\(str)"]
        var dictionary = NSMutableDictionary()
        dictionary.setObject(array, forKey: "TimeClock")
        
        dictionary.writeToFile(path, atomically: true)
        
    }
    
    func loadFromPlist(){
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let path = documentDirectory.stringByAppendingPathComponent("data.plist")
        
        var data = NSMutableDictionary(contentsOfFile: path)
        
        var keyDate: [String : [String]]!
        var key:[String]!
        var date:[String]!
        
        if (keyDate != nil){
            keyDate = NSDictionary(contentsOfFile: path) as [String:[String]]
            let keys = keyDate.keys
            key = sorted(keys)
            let selectedTime = key[0]
            date = keyDate[selectedTime]
            timeAlarm = date[0]
        }
    }
    
    func playSound(){
        if soundID == 0{
            let soundURL = NSBundle.mainBundle().URLForResource(
                "Alarm", withExtension: "wav")! as CFURLRef
            AudioServicesCreateSystemSoundID(soundURL, &soundID)
        }
        AudioServicesPlaySystemSound(soundID)
    }

}
