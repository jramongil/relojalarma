//
//  Alarma.swift
//  RelojAlarma
//
//  Created by Javier Ramon Gil on 14/4/15.
//  Copyright (c) 2015 Apress. All rights reserved.
//

import UIKit
import AudioToolbox

class Alarma: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    //var time: NSString?
    var timeClock = " "
    private var soundID: SystemSoundID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let date = NSDate()
        datePicker.setDate(date, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createAlarm(sender: AnyObject) {
        let dateOfPicker = datePicker.date
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "HH:mm, dd/MM/yy"
        
        /*let alert = UIAlertController(title: "Establecer alarma", message: "Alarma establecida para las \(dateformatter.stringFromDate(dateOfPicker))", preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)*/
        
        var str = dateformatter.stringFromDate(dateOfPicker)
        
        let time = (str as NSString).substringToIndex(5)
        
        loadFromPlist()
        
        if time == timeClock{
            //println("Coinciden los tiempos")
            
            self.playSound()
            let alert = UIAlertController(title: "Aviso", message: "Las horas de los pickers coinciden: \(timeClock)", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alert.addAction(action)
            presentViewController(alert, animated: true, completion: nil)
            
        }
        
        writeToPlist(time)

    }

    func writeToPlist(data:String){
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let path = documentDirectory.stringByAppendingPathComponent("data.plist")
        
        var array:NSArray = ["\(data)"]
        var dictionary = NSMutableDictionary()
        dictionary.setObject(array, forKey: "TimeAlarm")
        
        dictionary.writeToFile(path, atomically: true)
    }
    
    func loadFromPlist(){
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let path = documentDirectory.stringByAppendingPathComponent("data.plist")
        
        var data = NSMutableDictionary(contentsOfFile: path)
        
        var keyDate: [String : [String]]!
        var key:[String]!
        var date:[String]!

        keyDate = NSDictionary(contentsOfFile: path) as [String:[String]]
        let keys = keyDate.keys
        key = sorted(keys)
        let selectedTime = key[0]
        date = keyDate[selectedTime]
        timeClock = date[0]
        
    }
    
    func playSound(){
        if soundID == 0{
            let soundURL = NSBundle.mainBundle().URLForResource(
                "Alarm", withExtension: "wav")! as CFURLRef
            AudioServicesCreateSystemSoundID(soundURL, &soundID)
        }
        AudioServicesPlaySystemSound(soundID)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
